import { initializeApp } from "firebase/app";
import { getDatabase, ref} from 'firebase/database';


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCiQ8XqRQqtMeZ5ABmvXlRFrLIG2uQRWzI",
  authDomain: "todolist-a860a.firebaseapp.com",
  projectId: "todolist-a860a",
  storageBucket: "todolist-a860a.appspot.com",
  messagingSenderId: "166475703585",
  appId: "1:166475703585:web:059aaf5e260c5d354c4a60",
  measurementId: "G-4TFQ8XDEMQ"
};


export function firebaseSlugBase() {
  return ref(getDatabase())
}

// Initialize Firebase
export const firebaseApp = initializeApp(firebaseConfig);
export const slug = 'be94f5fcba'