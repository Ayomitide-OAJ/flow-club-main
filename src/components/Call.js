import DailyIframe from '@daily-co/daily-js';
import { child, onValue, set, remove, update } from '@firebase/database';
import { useEffect, useRef, useState } from 'react';
import { dailyParticipantInfo, makeParticipantUpdateHandler } from '../utils/daily';
import { firebaseSlugBase } from '../utils/firebase';
import { getRoomUrl } from '../utils/room';
import './Call.css'
import './todostyles.css'




function Call({firebaseApp}) {
    
    const callWrapperEl = useRef(null)
    const [participants, setParticipants] = useState({})
    const base = firebaseSlugBase();
    const callFrame = useRef(null)
    useEffect(() => {
        const roomUrl = getRoomUrl()
        const frame = DailyIframe.createFrame(
            callWrapperEl.current,
            {
                url: roomUrl
            }
        )

        callFrame.current = frame
        frame.join()
            .then(frameParticipants => {
                let newParticipants = {}
                for (const [id, participant] of Object.entries(frameParticipants)) {
                    newParticipants[id] = dailyParticipantInfo(participant)
                }
                setParticipants(newParticipants)
            })

        frame.on('participant-joined', makeParticipantUpdateHandler(setParticipants))
        frame.on('participant-updated', makeParticipantUpdateHandler(setParticipants))
        frame.on('participant-left', makeParticipantUpdateHandler(setParticipants))

        return () => {
            callFrame.current.leave()
            callFrame.current.destroy()
        }
    }, [])

    const [isEditingStatus, setIsEditingStatus] = useState(false)
    const [currentStatus, setCurrentStatus] = useState('')
    const [userStatuses, setUserStatuses] = useState({})

    /** Todo states declaration */
    const [todos, setTodos] = useState([]);
    const [todo, setTodo] = useState("");
    const [todoStatuses, setTodoStatuses] = useState({});
    const [todoEditing, setTodoEditing] = useState(null);
    const [editingText, setEditingText] = useState("");

    useEffect(() => {
        const base = firebaseSlugBase();
        const statusesRef = child(base, 'user_statuses')
        onValue(statusesRef, (snapshot) => {
            if (snapshot.val()) {
                setUserStatuses(snapshot.val())
                console.log(snapshot.val());
            }
        })
    }, [])

    useEffect(() => {
        const base = firebaseSlugBase();
        const todoRef = child(base, 'user_todo')
        onValue(todoRef, (snapshot) => {
            if (snapshot.val()){
                const todos = snapshot.val();

                const todoList = [];
                for (let id in todos){
                    todoList.push({ id, ...todos[id]});
                }
                setTodoStatuses(todoList);
                console.log(todoList);
            }
        })
    }, [])

    const localParticipant = Object.values(participants).find(participant => participant.isLocal)
    //const todoRef = child(base, `user_todo`);
    let values = Object.entries(participants);
    console.log("object entries participants", values);

    const finishEditing = () => {
        const base = firebaseSlugBase()
        setIsEditingStatus(false)
        if (localParticipant) {
            set(child(base, `user_statuses/${localParticipant.id}`), currentStatus)
        }
    }



    const deleteTodo = (id) => {
        const todoRef = child(base, 'user_todo', todo.id);
        let updatedTodos = [...todos].filter((todo) => todo.id !== id);
       // remove(todoRef, todo.id);
        setTodos(updatedTodos);
    }

    const toggleComplete = (id) => {
        const todoRef = child(base, 'user_todo', todo.id);
        
        let updatedTodos = [...todos].map((todo) => {
            if (todo.id === id){
                todo.completed = !todo.completed;
                update(todoRef, {
                    completed: !todo.completed
                })
            }
            return todo;
        });
        setTodos(updatedTodos);
    }

    console.log("other todos arrays", todoStatuses);
    
    const submitEdits = (id) => {
        const todoRef = child(base, 'user_todo', todo.id);
        const updatedTodos = [...todos].map((todo) => {
            if (todo.id === id){
                todo.text = editingText;
                update(todoRef, {
                    text: editingText,
                });
            }
            return todo;
        });

        setTodos(updatedTodos);
        setTodoEditing(null);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const base = firebaseSlugBase()
        const newTodo = {
            id: new Date().getTime(),
            text: todo,
            completed: false,
        }
        setTodos([...todos].concat(newTodo));
        if (localParticipant) {
            set(child(base, `user_todo/${localParticipant.id}`), todos);
        }
        setTodo("");
    }

    const getTypeOfValue = (value) => 
        Object.prototype.toString.call(value).slice(8, -1).toLowerCase();

    const objectHasInnerObject = (item) => {
        let response = false;
      // console.log(Object.keys(item));
      Object.keys(item).forEach((key) => {
          if (getTypeOfValue(item[key]) === "object"){
              response = true;
          }
      });
      return response;
    }   
    
    const relevantTodo = [];
    Object.entries(participants)
        .filter(([_, info]) => info.id !== localParticipant?.id)
        .map(([id, info]) => {
            for(let i=0; i<todoStatuses.length; i++){ 
                // console.log("gotten here todos", todoStatuses[i].id);
                // console.log("gotten here info", info.id);
                if(info.id === todoStatuses[i].id){
                    relevantTodo.push(todoStatuses[i]);
                    console.log("gotten here", relevantTodo);
                    break;
                }
            
            }
        });

    //console.log("relevantTodos", relevantTodo);
    // console.log("entries ::", Object.entries(participants).filter(([_, info]) => info.id !== localParticipant?.id))
    // console.log("entries 234 ::", Object.entries(participants).filter(([_, info]) => info.id !== localParticipant?.id).map(([id, info]) => {
    //         return {"name": info.name,
    //                 "status":!(id in userStatuses) ? '<empty>' : id in userStatuses && userStatuses[id].split('\n').map((v, i) => {
    //                     return {
    //                         "i": i,
    //                         "v": v
    //                     }
    //                 })
    //             }
    //         }))
    

    return (
        <div style={{ height: '100vh', minWidth: '100vh', display: 'flex' }}>
            <div
                id='call'
                ref={callWrapperEl}
                style={{ height: '100%', width: '70%' }}
            />
            <div style={{ width: '30%', padding: '10px' }}>
                <h2>Statuses</h2>
                <h3>My Status</h3>
                {localParticipant && (
                    <p>name: {localParticipant?.name}</p>
                )}
                {isEditingStatus ? (
                    <div style={{ display: 'flex', width: '100%', justifyContent: 'center' }}>
                        <textarea
                            rows={4}
                            onChange={(ev) => setCurrentStatus(ev.target.value)}
                            onBlur={() => finishEditing()}
                            value={currentStatus}
                        />
                        <p onBlur={() => finishEditing()} className='cursor-pointer'>✔️</p>
                    </div>
                ) : (
                    <div className='cursor-pointer' onClick={() => setIsEditingStatus(true)}>
                        {currentStatus ? currentStatus.split('\n').map((v, i) => (
                            <p key={v + i}>{i === 0 && '✏️'}{v}</p>
                        )) : (
                            <p>✏️&lt;empty&gt;</p>

                        )}
                    </div>
                )}
                <h3>Other Statuses</h3>
                {Object.entries(participants)
                    .filter(([_, info]) => info.id !== localParticipant?.id)
                    .map(([id, info]) => (
                        <div key={id} style={{ border: '1px solid gray' }}>
                            <p><strong>name:</strong> {info.name}</p>
                            <p><strong>status:</strong>{!(id in userStatuses) && '<empty>'}</p>
                            {id in userStatuses && userStatuses[id].split('\n').map((v, i) => (
                                <p key={v + i}>{v}</p>
                            ))
                            }
                        </div>
                    ))
                }
                <h3>Other Todos</h3>
                {/* {Object.entries(participants)
                    .filter(([_, info]) => info.id !== localParticipant?.id)
                    .map(([id, info]) => (
                        <div key={id} style={{ border: '1px solid gray' }}>  
                            <p><strong>name: </strong> {info.name}</p>
                            <p><strong>todos: </strong> </p>
                            { todoStatuses && todoStatuses.map(({id, text}) => (
                                <p key={id}> {text}</p>
                                
                             ))}
                        
                        </div>    
                    ))  
                 } */}
                        {Object.entries(participants)
                            .filter(([_, info]) => info.id !== localParticipant?.id)
                            .map(([id, info]) => (
                                <div key={id} style={{ border: '1px solid gray' }}>
                                    <p><strong>name:</strong> {info.name}</p>
                                    <p><strong>todos</strong></p>
                                    { relevantTodo &&
                                       relevantTodo.map((item)=> 
                                         objectHasInnerObject(item) === false ? (
                                            <p key={item.id}> {item.text}</p>
                                         ):(
                                            Object.keys(item).map((_key) => (
                                                <p key={item[_key].id}>{item[_key].text}</p>
                                            ))
                                         ))
                                        
                                    }
                                    
                                   
                                    
                                </div>
                            ))
                            }
                           
                                 
                <h2>Todos</h2>
                
        <div id="todo-list">

            <h1>Todo List</h1>

            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    onChange={(e) => setTodo(e.target.value)}
                    value={todo}
                />
                 <button type="submit">Add Todo</button>
            </form>
            {todos.map((todo) => (
            <div key={todo.id} className="todo">
                <div className="todo-text">
                <input
                    type="checkbox"
                    id="completed"
                    checked={todo.completed}
                    onChange={() => toggleComplete(todo.id)}
                />
                {todo.id === todoEditing ? (
                  <input
                    type="text"
                    onChange={(e) => setEditingText(e.target.value)}
                    />
                ) : (
                    <div>{todo.text}</div>
                )}
                </div>
                <div className="todo-actions">
                    {todo.id === todoEditing ? (
                        <button onClick={() => submitEdits(todo.id)}>Submit Edits</button>
                    ) : (
                        <button onClick={() => setTodoEditing(todo.id)}>Edit</button>
                    )}
        
                    <button onClick={() => deleteTodo(todo.id)}>Delete</button>
                </div>
          </div>
        ))}
      </div>
              
    </div>
    </div >
    )
}

export default Call